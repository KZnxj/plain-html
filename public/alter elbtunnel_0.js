// Qgis2threejs Project
project = new Q3D.Project({crs:"EPSG:25832",wgs84Center:{lat:53.5427549086,lon:9.96915681188},proj:"+proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",title:"alter elbtunnel",baseExtent:[563372.23,5932697.87449,565069.796018,5933476.47731],rotation:0,zShift:0.0,width:100.0,zExaggeration:5.0});

// Layer 0
lyr = project.addLayer(new Q3D.DEMLayer({q:1,shading:true,type:"dem",name:"NE Bl7_025m_KN_LS320.xyz.cdf.25832.filled.clipped"}));
